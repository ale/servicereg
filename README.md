servicereg
==========

Register services with
[SkyDNS](https://github.com/skynetservices/skydns) by interfacing with
the local systemd.

To register a service running on this host, create a JSON-encoded file
in `/etc/servicereg`, using the LSB naming convention (so, no dots in
the file name). This should contain the name of the monitored service
and the port it runs on, for example:

    {
        "service": "etcd.service",
        "port": 2379
    }

This will register a service named *etcd* running on port 2679. The
`.service` suffix is automatically removed from the service name to
generate the DNS name, or you can set the `name` attribute explicitly.

When the *etcd.service* systemd unit is active, servicereg will create
an object in etcd at `/skydns/DOMAIN/etcd/HOSTNAME` (depending on your
settings for the domain and hostname), pointing at the local host,
which will be picked up by SkyDNS. As long as the unit is running, the
object will be kept fresh with a periodic heartbeat request. Services
that are no longer running will be removed by etcd itself when the TTL
on the DNS object expires (5 seconds by default).

Options can be set with command line flags, or using environment
variables named as the uppercase flag with a `SERVICEREG_` prefix.

The program should be run as root if you want to bypass DBus and use a
direct connection to systemd. Use the `--use-dbus` flag otherwise.
