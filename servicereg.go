package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	etcdclient "github.com/coreos/etcd/client"
	"github.com/coreos/etcd/pkg/transport"
	"github.com/coreos/go-systemd/dbus"
	"golang.org/x/exp/inotify"
	"golang.org/x/net/context"
)

var (
	domainName       = flag.String("domain", "", "domain name to advertise")
	configDir        = flag.String("config-dir", "/etc/servicereg", "config directory")
	etcdSRV          = flag.String("etcd-srv", "", "etcd DNS SRV lookup host")
	etcdURLs         = flag.String("etcd-urls", "http://localhost:2379", "etcd URLs (comma separated)")
	etcdSSLCert      = flag.String("etcd-ssl-cert", "", "SSL client certificate")
	etcdSSLKey       = flag.String("etcd-ssl-key", "", "SSL client private key")
	etcdSSLCA        = flag.String("etcd-ssl-ca", "", "SSL CA")
	hostTag          = flag.String("tag", localHostname(), "host tag")
	publicIP         = flag.String("ip", localIP(), "public IP for this host")
	presenceTTL      = flag.Duration("presence-ttl", 10*time.Second, "presence TTL, should be > period")
	presenceInterval = flag.Duration("presence-interval", 5*time.Second, "presence interval, how often to ping etcd")
	debug            = flag.Bool("debug", false, "enable debug messages")

	etcdTimeout        = 3 * time.Second
	etcdConnectTimeout = 10 * time.Second
)

// Try to guess this machine's IP.
func localIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, addr := range addrs {
		// Parse the network, retain the assigned IP.
		ip, _, err := net.ParseCIDR(addr.String())
		if err != nil || ip.IsLoopback() || ip.IsMulticast() || ip.IsLinkLocalMulticast() || ip.IsInterfaceLocalMulticast() {
			continue
		}
		return ip.String()
	}
	return ""
}

// Try to find the local host name.
func localHostname() string {
	if hostname, err := os.Hostname(); err == nil {
		if n := strings.Index(hostname, "."); n >= 0 {
			return hostname[:n]
		}
		return hostname
	}
	return ""
}

func newEtcdClientFromFlags(ctx context.Context) (etcdclient.Client, error) {
	var tls transport.TLSInfo

	if *etcdSSLCert != "" && *etcdSSLKey != "" && *etcdSSLCA != "" {
		tls.CAFile = *etcdSSLCA
		tls.CertFile = *etcdSSLCert
		tls.KeyFile = *etcdSSLKey
		if *etcdSRV != "" {
			tls.ServerName = *etcdSRV
		}
	}

	t, err := transport.NewTransport(tls, etcdConnectTimeout)
	if err != nil {
		return nil, err
	}

	var endpoints []string
	if *etcdSRV != "" {
		var err error
		endpoints, err = etcdclient.NewSRVDiscover().Discover(*etcdSRV)
		if err != nil {
			return nil, err
		}
	} else {
		endpoints = strings.Split(*etcdURLs, ",")
	}

	c, err := etcdclient.New(etcdclient.Config{
		Transport:               t,
		Endpoints:               endpoints,
		HeaderTimeoutPerRequest: 3 * time.Second,
	})
	if err != nil {
		return nil, err
	}

	// Start an autosync goroutine in the background.
	go func() {
		autoSyncPeriod := 30 * time.Second
		for {
			err := c.AutoSync(ctx, autoSyncPeriod)
			if err == context.Canceled {
				break
			}
			log.Printf("autosync error: %v", err)
			time.Sleep(autoSyncPeriod)
		}
	}()

	return c, nil
}

// Service registration config.
type Service struct {
	// Systemd unit name, including the ".service" suffix.
	Service string `json:"service"`

	// Name to be registered in DNS. If empty, use the systemd
	// unit name stripped of the ".service" suffix.
	Name string `json:"name,omitempty"`

	// Port the service runs on.
	Port int `json:"port"`

	// Tag is the value of the left-most DNS prefix. If empty,
	// defaults to the local hostname.
	Tag string `json:"tag,omitempty"`

	// The RFC2782 service, if present, triggers the creation of a
	// RFC2782-compatible SRV record in SkyDNS, of which it is the
	// left-most component.
	RFC2782Service string `json:"rfc2782_service,omitempty"`
}

func (s Service) Valid() bool {
	return s.Service != "" && s.Port > 0
}

type DNSEntry struct {
	Path string `json:"-"`
	Host string `json:"host"`
	Port int    `json:"port"`

	serialized string
	created    bool
}

func NewDNSEntry(path, host string, port int) *DNSEntry {
	return &DNSEntry{
		Path: path,
		Host: host,
		Port: port,

		// By defaulting to create: true, we minimize the
		// amount of writes to the database that happen on
		// config reload - we assume that the entry has
		// already been created, and reset it only if its
		// content differs.
		created: true,
	}
}

func (e *DNSEntry) Serialize() string {
	if e.serialized == "" {
		data, _ := json.Marshal(e)
		e.serialized = string(data)
	}
	return e.serialized
}

func dnsPath(domain string) string {
	parts := strings.Split(domain, ".")
	tmp := []string{""}
	for i := len(parts) - 1; i >= 0; i-- {
		tmp = append(tmp, parts[i])
	}
	return strings.Join(tmp, "/")
}

func stripService(s string) string {
	return strings.TrimSuffix(s, ".service")
}

func makeDNSEntries(service *Service) []*DNSEntry {
	tag := service.Tag
	if tag == "" {
		tag = *hostTag
	}
	name := service.Name
	if name == "" {
		name = stripService(service.Service)
	}
	path := dnsPath(fmt.Sprintf("%s.%s.%s", tag, name, *domainName))
	var out []*DNSEntry
	out = append(out, NewDNSEntry(path, *publicIP, service.Port))
	if service.RFC2782Service != "" {
		path = dnsPath(fmt.Sprintf("%s.%s._tcp.%s.%s", tag, service.RFC2782Service, name, *domainName))
		out = append(out, NewDNSEntry(path, *publicIP, service.Port))
	}
	return out
}

const updateConcurrency = 5

var updateSem = make(chan struct{}, updateConcurrency)

func updatePresence(ctx context.Context, keysAPI etcdclient.KeysAPI, entry *DNSEntry) {
	updateSem <- struct{}{}
	defer func() {
		<-updateSem
	}()

	cctx, cancel := context.WithTimeout(ctx, etcdTimeout)
	defer cancel()

	key := "/skydns" + entry.Path

	var err error
	if !entry.created {
		// Create the entry no matter what its current state might be.
		_, err = keysAPI.Set(cctx, key, entry.Serialize(), &etcdclient.SetOptions{
			TTL: *presenceTTL,
		})
	} else {
		_, err = keysAPI.Set(cctx, key, "", &etcdclient.SetOptions{
			TTL:       *presenceTTL,
			PrevExist: etcdclient.PrevExist,
			PrevValue: entry.Serialize(),
			Refresh:   true,
		})
	}
	if err != nil {
		log.Printf("error updating %s: %v", key, err)
		entry.created = false
	} else {
		entry.created = true
	}
}

func runPresence(ctx context.Context, etcd etcdclient.Client, ch <-chan dnsOp) {
	keysAPI := etcdclient.NewKeysAPI(etcd)
	entries := make(map[string][]*DNSEntry)
	tick := time.NewTicker(*presenceInterval)
	for {
		select {
		case <-tick.C:
			var wg sync.WaitGroup
			for _, ee := range entries {
				for _, e := range ee {
					wg.Add(1)
					go func(e *DNSEntry) {
						updatePresence(ctx, keysAPI, e)
						wg.Done()
					}(e)
				}
			}
			wg.Wait()

		case op := <-ch:
			if op.ok {
				entries[op.service.Service] = makeDNSEntries(op.service)
				if *debug {
					log.Printf("updated dns entry for %s", op.service.Service)
				}
			} else {
				delete(entries, op.service.Service)
				if *debug {
					log.Printf("deleted dns entry for %s", op.service.Service)
				}
			}

		case <-ctx.Done():
			return
		}
	}
}

type dnsOp struct {
	service *Service
	ok      bool
}

// Listen to systemd events, and to configuration updates.
func watchSystemd(ctx context.Context, configCh <-chan map[string]*Service, opCh chan dnsOp) {
	// SubscribeUnitsCustom just periodically lists all units and
	// returns the ones we haven't seen yet (that match our
	// filter), so we can just update the filter whenever there is
	// a configuration change. If we remove an entry from the
	// filter set, SubscribeUnitsCustom will send us a delete
	// event.
	conn, err := dbus.NewSystemdConnection()
	if err != nil {
		log.Fatalf("error connecting to systemd: %v", err)
	}

	if err := conn.Subscribe(); err != nil {
		log.Fatalf("error in dbus.Subscribe(): %v", err)
	}

	// Use a struct to share the current services map between
	// goroutines.
	var state struct {
		services map[string]*Service
	}
	var mx sync.Mutex

	// Note that the semantics of the filter function are the opposite
	// of what one would normally expect: it should return true for
	// units that should be ignored, false for the ones we care about.
	updateCh, errCh := conn.SubscribeUnitsCustom(time.Second, 256, compareUnitStatus, func(unit string) bool {
		mx.Lock()
		defer mx.Unlock()
		if state.services == nil {
			return true
		}
		_, ok := state.services[unit]
		return !ok
	})

	for {
		select {
		case services := <-configCh:
			mx.Lock()
			log.Printf("configuration updated, watching %d services", len(services))
			state.services = services
			mx.Unlock()

		case updateMap := <-updateCh:
			for unit, status := range updateMap {
				mx.Lock()
				svc, ok := state.services[unit]
				mx.Unlock()
				if !ok {
					// Service map has changed since we last
					// invoked the filter function, ignore.
					continue
				}
				unitOk := false
				if status == nil {
					if *debug {
						log.Printf("systemd update: unit=%s, state=deleted", unit)
					}
				} else {
					unitOk = status.ActiveState == "active"
					if *debug {
						log.Printf("systemd update: unit=%s, state=%s", unit, status.ActiveState)
					}
				}
				opCh <- dnsOp{
					service: svc,
					ok:      unitOk,
				}
			}

		case err := <-errCh:
			log.Printf("systemd error: %v", err)

		case <-ctx.Done():
			if conn != nil {
				conn.Close()
			}
			return
		}
	}
}

func compareUnitStatus(u1, u2 *dbus.UnitStatus) bool {
	return u1.Name != u2.Name ||
		u1.LoadState != u2.LoadState ||
		u1.ActiveState != u2.ActiveState ||
		u1.SubState != u2.SubState
}

func loadConfig(dir string) (map[string]*Service, error) {
	d, err := os.Open(dir)
	if err != nil {
		log.Printf("opening %s: %v", dir, err)
		return nil, err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		log.Printf("could not read directory %s: %v", dir, err)
		return nil, err
	}

	svcmap := make(map[string]*Service)
	for _, name := range names {
		if strings.Contains(name, ".") {
			continue
		}
		data, err := ioutil.ReadFile(filepath.Join(dir, name))
		if err != nil {
			log.Printf("error reading %s: %v", name, err)
			continue
		}
		var svc Service
		if err := json.Unmarshal(data, &svc); err != nil {
			log.Printf("syntax error reading %s: %v", name, err)
			continue
		}
		if !svc.Valid() {
			log.Printf("error reading %s: invalid (empty) service", name)
			continue
		}
		svcmap[svc.Service] = &svc
	}
	return svcmap, nil
}

// A bitmask of inotify events that imply that directory contents
// might have changed.
var writeEventMask = (inotify.IN_CLOSE_WRITE | inotify.IN_DELETE | inotify.IN_MOVED_FROM | inotify.IN_MOVED_TO)

func isInterestingEvent(ev *inotify.Event) bool {
	return (ev.Mask & writeEventMask) != 0
}

func watchConfig(ctx context.Context, dir string, ch chan map[string]*Service) {
	// Setup errors are fatal.
	watcher, err := inotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()
	if err = watcher.Watch(dir); err != nil {
		log.Fatal(err)
	}

	// Load the initial configuration.
	if services, err := loadConfig(dir); err == nil {
		ch <- services
	}

	for {
		select {
		case err = <-watcher.Error:
			log.Printf("inotify error: %v", err)
		case ev := <-watcher.Event:
			if isInterestingEvent(ev) {
				log.Printf("detected config change, reloading...")
				if services, err := loadConfig(dir); err == nil {
					ch <- services
				}
			}
		case <-ctx.Done():
			return
		}
	}
}

// Set defaults for command-line flags using variables from the environment.
func setDefaultFlagsFromEnv() {
	flag.VisitAll(func(f *flag.Flag) {
		envVar := "SERVICEREG_" + strings.ToUpper(strings.Replace(f.Name, "-", "_", -1))
		if value := os.Getenv(envVar); value != "" {
			f.DefValue = value
			if err := f.Value.Set(value); err != nil {
				log.Printf("Warning: invalid value for %s: %v", envVar, err)
			}
		}
	})
}

func main() {
	log.SetFlags(0)
	setDefaultFlagsFromEnv()
	flag.Parse()

	// Verify that required args are set.
	if *domainName == "" {
		log.Fatal("--domain must be specified")
	}
	if *hostTag == "" {
		log.Fatal("--tag must be specified")
	}
	if *publicIP == "" {
		log.Fatal("--ip must be specified")
	}

	// Create a global Context that we can cancel on termination.
	ctx, cancel := context.WithCancel(context.Background())

	// Connect to etcd.
	etcd, err := newEtcdClientFromFlags(ctx)
	if err != nil {
		log.Fatal("could not connect to etcd:", err)
	}

	// Gently terminate on SIGTERM and friends.
	sigTermCh := make(chan os.Signal, 1)
	go func() {
		<-sigTermCh
		cancel()
	}()
	signal.Notify(sigTermCh, syscall.SIGTERM, syscall.SIGINT)

	// Watch the configuration and setup the systemd watcher
	// accordingly.
	ch := make(chan map[string]*Service, 1)
	opCh := make(chan dnsOp, 10)
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		watchConfig(ctx, *configDir, ch)
		wg.Done()
	}()
	go func() {
		watchSystemd(ctx, ch, opCh)
		wg.Done()
	}()
	go func() {
		runPresence(ctx, etcd, opCh)
		wg.Done()
	}()
	wg.Wait()
}
